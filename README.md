## How To Install COLMAP on Managed Machines

COLMAP is installed as part of [https://github.com/microsoft/vcpkg.](https://github.com/microsoft/vcpkg.) The installation process [https://colmap.github.io/install.html#vcpkg](https://colmap.github.io/install.html#vcpkg) was buggy and required additional installs to support qt-5. See the installation steps below for a less painful experience.

### Environment

OS: Ubuntu 18.04

Required: SUDOERS Permissions

### Installation

```plaintext
> sudo apt-get install '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev
> git clone https://github.com/microsoft/vcpkg
> cd vcpkg
> ./bootstrap-vcpkg.sh
> ./vcpkg install colmap:x64-linux
```

### Usage

```plaintext
> cd /path/to/vcpkg/
> ./packages/colmap_x64-linux/tools/colmap/colmap
```
